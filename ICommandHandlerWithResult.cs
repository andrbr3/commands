﻿namespace Framework.Commands
{
	public interface ICommandHandlerWithResult<TResult>
	{
		TResult GetResult();
	}
}