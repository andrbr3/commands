namespace Framework.Commands
{
	public enum CommandStatus
	{
		Unknown,
		Executing,
		Applied,
		Failed
	}
}