﻿using Cysharp.Threading.Tasks;

namespace Framework.Commands
{
	public interface ICommandExecutor
	{
		bool Validate(IAbstractCommandHandler commandHandler, out Error error);

		UniTask Execute(IAbstractCommandHandler commandHandler);

		UniTask<TResult> Execute<TResult>(IAbstractCommandHandlerResult<TResult> commandHandler);
	}
}