﻿namespace Framework.Commands
{
	public interface IGameCommandHandler<in TCommand> : IAbstractCommandHandler, ICommandHandler<TCommand> where TCommand : struct
	{
		
	}
}