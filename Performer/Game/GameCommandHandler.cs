﻿using System;
using Framework.Commands;

namespace Framework
{
	public abstract class GameCommandHandler<TCommand> : CommandHandler<TCommand>, IGameCommandHandler<TCommand> where TCommand : struct
	{
		public virtual bool IsDev => false;
		public virtual bool IsImportant => false;
		public override Type ExecutorType => typeof(GameCommandExecutor);
	}

	public abstract class GameCommandHandler<TCommand, TResult> : CommandHandler<TCommand, TResult>, IGameCommandHandler<TCommand>
		where TCommand : struct, ICommandWithResult<TResult>
	{
		public virtual bool IsDev => false;
		public virtual bool IsImportant => false;
		public override Type ExecutorType => typeof(GameCommandExecutor);
	}
}