﻿using Cysharp.Threading.Tasks;
using Framework;

namespace Framework.Commands
{
	public class GameCommandExecutor : ICommandExecutor
	{
		public bool Validate(IAbstractCommandHandler commandHandler, out Error error) => commandHandler.Validate(out error);
		public virtual UniTask Execute(IAbstractCommandHandler commandHandler)
		{
			return commandHandler.Execute();
		}

		public virtual UniTask<TResult> Execute<TResult>(IAbstractCommandHandlerResult<TResult> commandHandler)
		{
			return commandHandler.ExecuteWithResult();
		}
	}
}