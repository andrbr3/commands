﻿using Cysharp.Threading.Tasks;
using UniRx;
using Zenject;
using Logger = Framework.Logging.Logger;

namespace Framework.Commands
{
	public class Performer : IPerformer
	{
		[Inject] private readonly Logger _logger;
		[Inject] private readonly DiContainer _container;

		public async UniTask Perform<TCommand>(TCommand command) where TCommand : struct
		{
			var factory = _container.Resolve<ICommandFactory<TCommand>>();
			var commandHandler = factory.CreateHandler();
			var executor = factory.ResolveExecutor();

			_logger.Trace($"Performing: {typeof(TCommand).Name}");
			using var disposable = new CompositeDisposable();
			if (executor == null)
			{
				throw new ErrorException($"Performing failed: unable to find HandlerExecutor {typeof(TCommand).Name}");
			}

			if (!InitHandler(command, commandHandler,out Error initError))
			{
				throw new InitException(initError);
			}

			if (!executor.Validate(commandHandler, out Error error))
			{
				throw new ValidateException(error);
			}
			
			if (commandHandler is IAbstractCommandHandler commandHandlerTyped)
			{
				await executor.Execute(commandHandlerTyped);
				return;
			}
				
			throw new ErrorException($"Performing failed: unable to cast HandlerExecutor {typeof(TCommand).Name}");

		}

		private bool InitHandler<TCommand>(TCommand command, ICommandHandler<TCommand> commandHandler, out Error error) where TCommand : struct
		{
			if (!commandHandler.Init(command))
			{
				error = new Error($"Initialization of \"{command}\" failed");
				return false;
			}

			error = default;
			return true;
		}

		public async UniTask<TResult> Perform<TCommand, TResult>(TCommand command) where TCommand : struct, ICommandWithResult<TResult>
		{
			var factory = _container.Resolve<ICommandFactory<TCommand>>();
			var commandHandler = factory.CreateHandler();
			var executor = factory.ResolveExecutor();
			
			_logger.Trace($"Performing: {typeof(TCommand).Name}");
			using var disposable = new CompositeDisposable();
			if (executor == null)
			{
				throw new ErrorException($"Performing failed: unable to find HandlerExecutor {typeof(TCommand).Name}");
			}

			if (!InitHandler(command, commandHandler,out Error initError))
			{
				throw new InitException(initError);
			}

			if (!executor.Validate(commandHandler, out Error error))
			{
				throw new ValidateException(error);
			}
			
			if (commandHandler is IAbstractCommandHandlerResult<TResult> commandHandlerTyped)
			{
				return await executor.Execute(commandHandlerTyped);
			}

			throw new ErrorException($"Performing failed: unable to find HandlerExecutor {typeof(TCommand).Name}");
		}
	}
}