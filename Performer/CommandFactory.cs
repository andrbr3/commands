using UnityEngine.Scripting;
using Zenject;

namespace Framework.Commands
{
    public interface ICommandFactory<in TCommand> where TCommand : struct
    {
        ICommandHandler<TCommand> CreateHandler();
        ICommandExecutor ResolveExecutor();
    }
    
    [Preserve]
    public class CommandFactory<TCommand, THandler, TExecutor>: ICommandFactory<TCommand> 
        where TCommand : struct 
        where THandler : ICommandHandler<TCommand>, new()
        where TExecutor: ICommandExecutor
    {
        [Inject] private readonly DiContainer _container;
        
        public ICommandHandler<TCommand> CreateHandler()
        {
            var handler = new THandler();
            _container.Inject(handler);
            return handler;
        }

        public ICommandExecutor ResolveExecutor()
        {
            return _container.Resolve<TExecutor>();
        }
    }
}