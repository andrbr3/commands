﻿using System;
using Cysharp.Threading.Tasks;
using UnityEngine;

namespace Framework.Commands
{

	public interface IPerformer
	{
		UniTask Perform<TCommand>(TCommand command) where TCommand : struct;

		UniTask<TResult> Perform<TCommand, TResult>(TCommand command) where TCommand : struct, ICommandWithResult<TResult>;

		public async UniTask<(bool successful, Exception exception)> TryPerform<TCommand>(TCommand command) where TCommand : struct
		{
			try
			{
				await Perform(command);
				return (true, default);
			}
			catch (ValidateException e)
			{
				return (false, e);
			}
			catch (Exception e)
			{
				Debug.LogError(e);
				return (false, e);
			}
		}

		public async UniTask<(bool successful, TResult value, Exception exception)> TryPerform<TCommand, TResult>(TCommand command) where TCommand : struct, ICommandWithResult<TResult>
		{
			try
			{
				TResult result = await Perform<TCommand, TResult>(command);
				return (true, result, default);
			}
			catch (ValidateException e)
			{
				return (false, default, e);
			}
			catch (Exception e)
			{
				Debug.LogError(e);
				return (false, default, e);
			}
		}
	}
}