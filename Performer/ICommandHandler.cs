﻿using System;
using Cysharp.Threading.Tasks;

namespace Framework.Commands
{
	public interface IAbstractCommandHandler
	{
		Type ExecutorType { get; }
		bool Validate();
		bool Validate(out Error error);
		UniTask Execute();
	}

	public interface IAbstractCommandHandlerResult<TResult>: IAbstractCommandHandler
	{
		async UniTask IAbstractCommandHandler.Execute()
		{
			await ExecuteWithResult();
		}

		UniTask<TResult> ExecuteWithResult();
	}
	
	public interface ICommandHandler<in TCommand> : IAbstractCommandHandler, IInitializable<TCommand> where TCommand : struct { }
	public interface ICommandHandlerResult<in TCommand, TResult> : ICommandHandler<TCommand>, IAbstractCommandHandlerResult<TResult>, IInitializable<TCommand> where TCommand : struct, ICommandWithResult<TResult> { }
}