﻿using Cysharp.Threading.Tasks;
using Framework;

namespace Framework.Commands
{
	public class HttpCommandExecutor : ICommandExecutor
	{
		public bool Validate(IAbstractCommandHandler commandHandler, out Error error)
		{
			error = default;
			return true;
		}
		
		public virtual UniTask Execute(IAbstractCommandHandler commandHandler)
		{
			return commandHandler.Execute();
		}

		public virtual UniTask<TResult> Execute<TResult>(IAbstractCommandHandlerResult<TResult> commandHandler)
		{
			return commandHandler.ExecuteWithResult();
		}
	}
}