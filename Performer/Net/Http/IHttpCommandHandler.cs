﻿using System;
using Framework;

namespace Framework.Commands
{
	public interface IAbstractHttpCommandHandler : IAbstractCommandHandler
	{
		Type Service { get; }
		string GetCommandUri();
		
		byte[] EncodeRequest();
		bool DecodeResponse(byte[] dataBytes);
		CustomDictionary<string, string> GetAdditionalHeaders();
	}

	public interface IHttpCommandHandler<in TCommand, TRequest, TResponse>
		: IAbstractHttpCommandHandler
		, ICommandHandler<TCommand>
		where TCommand : struct
		where TRequest : struct, IBinarySerializable
		where TResponse : struct, IBinaryDeserializable
	{ }
}