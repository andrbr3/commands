﻿using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using Framework;
using Zenject;

namespace Framework.Commands
{
	public abstract class HttpCommandHandler<TCommand, TRequest, TResponse>
		: CommandHandler<TCommand>
		, IHttpCommandHandler<TCommand, TRequest, TResponse>
		where TCommand : struct
		where TRequest : struct, IBinarySerializable
		where TResponse : struct, IBinaryDeserializable
	{
		private const int MaxAttemptsCount = 5;
		[Inject] private readonly CurrentCluster _currentCluster;
		
		public override Type ExecutorType => typeof(HttpCommandExecutor);
		public abstract Type Service { get; }
		
		protected abstract TRequest GetRequest();
		public abstract string GetCommandUri();
		public abstract CustomDictionary<string, string> GetAdditionalHeaders();

		protected abstract bool HandleResponse(in TResponse response);
		
		public byte[] EncodeRequest()
		{
			var request = GetRequest();
			var binarySerializer = new BinarySerializer(new List<byte>());
			request.Serialize(binarySerializer);
			return binarySerializer.GetBytes();
		}

		public bool DecodeResponse(byte[] dataBytes)
		{
			var response = new TResponse();
			if (dataBytes != null)
			{
				var binaryDeserializer = new BinaryDeserializer(dataBytes);
				response.Deserialize(binaryDeserializer);
			}
			return HandleResponse(in response);
		}

		protected override async UniTask HandleExecute()
		{
			var request = EncodeRequest();
			var commandUri = GetCommandUri();
			var additionalHeaders = GetAdditionalHeaders();

			if (_currentCluster.TryGetService(Service, out IHttpService httpService))
			{
				var httpServiceRequest = new HttpServiceRequest(request, commandUri, additionalHeaders);
				
				var response = await httpService.Send(httpServiceRequest, MaxAttemptsCount);
				if (DecodeResponse(response.EncodedResponse) == false)
				{
					throw new Exception($"{httpServiceRequest.Uri} response decode failed");
				}
			}
			else
			{
				throw new Exception($"{Service} cluster not found");
			}
		}
	}
}