﻿using System;
using Cysharp.Threading.Tasks;
using UnityEngine;
using Zenject;

namespace Framework.Commands
{
	public abstract class CommandHandler<TCommand> : AbstractCommandHandler<TCommand>, IAbstractCommandHandler where TCommand : struct
	{
		protected abstract UniTask HandleExecute();

		public override async UniTask Execute()
		{
			if (!Validate(out var error))
			{
				throw new ErrorException(error);
			}

			try
			{
				await HandleExecute();
				HandleApply();
			}
			catch (Exception e)
			{
				HandleFail(e);
				throw;
			}
		}
	}

	public abstract class CommandHandler<TCommand, TResult> : AbstractCommandHandler<TCommand>, ICommandHandlerResult<TCommand, TResult>
		where TCommand : struct, ICommandWithResult<TResult>
	{
		protected abstract UniTask<TResult> HandleExecute();

		public override UniTask Execute() => ExecuteWithResult();

		public async UniTask<TResult> ExecuteWithResult()
		{
			if (!Validate(out var error))
			{
				throw new ErrorException(error);
			}

			try
			{
				var result = await HandleExecute();
				HandleApply();
				return result;
			}
			catch (Exception e)
			{
				HandleFail(e);
				throw;
			}
		}
	}

	public abstract class AbstractCommandHandler<TCommand> : ICommandHandler<TCommand> where TCommand : struct
	{
		[Inject] private readonly IPerformer _performer;
		protected TCommand Command;

		public abstract Type ExecutorType { get; }

		public bool Init(TCommand command)
		{
			Command = command;
			return HandleInit();
		}

		public bool Validate() => Validate(out _);

		public virtual bool Validate(out Error error)
		{
			error = default;
			return true;
		}

		public abstract UniTask Execute();

		protected virtual bool HandleInit() => true;

		protected virtual void HandleApply() { }

		protected virtual void HandleFail(Exception e)
		{
			Debug.LogError($"[{typeof(TCommand).Name}] {e.Message}");
		}

		protected Error Error(string message) => Error(0, message);

		protected Error Error(int code, string message) => new Error(code, message, GetType().Name);

		protected ErrorException Exception(string message)
		{
			return Exception(0, message);
		}

		protected ErrorException Exception(int code, string message)
		{
			return new ErrorException(code, message, GetType().Name);
		}
	}
}